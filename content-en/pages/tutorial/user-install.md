Title: User install
Template: doc

# User install

## From Ubuntu 19.04

Open a terminal and type:

```bash
sudo apt install silkaj
```

## From Debian Buster

Open a terminal and type:

```bash
sudo apt install silkaj
```

## From PyPI

To install, open a terminal and type:

```bash
pip3 install silkaj --user
```

To upgrade, open a terminal and type:

```bash
pip3 install silkaj --user --upgrade
```
