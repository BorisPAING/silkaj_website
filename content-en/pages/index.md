Title: Silkaj
BodyId: home

<section id="showcase">
	<div>
		<figure id="home-app-screenshot">
			<!--
			<img src="Silkaj-screenshot.png" 
				 alt="Screenshot of Silkaj" />
			-->
<pre><span></span><div>moul@duniter:~$ silkaj

	             @@@@@@@@@@@@@
         @@@     @         @@@
      @@@   @@       @@@@@@   @@.
     @@  @@@       @@@@@@@@@@@  @@
   @@  @@@       &amp;@@@@@@@@@@@@@  @@@
  @@  @@@       @@@@@@@@@#   @@@@ @@(
  @@ @@@@      @@@@@@@@@      @@@  @@
 @@  @@@      @@@@@@@@ @       @@@  @@
 @@  @@@      @@@@@@ @@@@       @@  @@ 
 @@  @@@@      @@@ @@@@@@@      @@  @@
  @@ @@@@*       @@@@@@@@@      @# @@
  @@  @@@@@    @@@@@@@@@@       @ ,@@
   @@  @@@@@ @@@@@@@@@@        @ ,@@
    @@@  @@@@@@@@@@@@        @  @@*
      @@@  @@@@@@@@        @  @@@
        @@@@   @@          @@@,
            @@@@@@@@@@@@@@@
</div></pre>
		</figure>

		<h2 id="USP">
			Powerfull and lightweight command line client for Duniter
		</h2>

		<p class="CTA-button">
			<a href="download">
				Install Silkaj 0.2.0			</a>
		</p>


		<p id="licence">
			Silkaj is free software distributed<br />under GNU AGPL-3.0 licence		</p>
	</div>
</section>


<section class="features-list" id="features-list-1">
	<div>
		<h2>
			Silkaj : the lightweight client
		</h2>
		
		<dl>
			<dt>
				Written in Python
			</dt>

			<dd>
				Silkaj is written with Python3.			</dd>

			<dt>
				It's on us ;-)			</dt>

			<dd>
				Because it is programmed with love by a community of people who believe in libre moneys in general (and in the Ğ1 in particular), Silkaj is given to you without demanding any counter-part.			</dd>

			<dt>
				You are Free too			</dt>

			<dd>
				Silkaj is a free software.This means you can have a look at its source code (<a href="https://git.duniter.org/clients/python/silkaj">on GitLab</a>) and adapt it to your need in order to, for instance, create your own free money. 			</dd>
		</dl>
	</div>
</section>
