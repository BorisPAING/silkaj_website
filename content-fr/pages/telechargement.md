Title: Télécharger Silkaj

<section id="download">

<section>
	<img class="card-img-top" src="/img/download/ubuntu-logo.svg" alt="Logo Ubuntu" />

	<h3>
		Ubuntu
	</h3>

	<p>Lancez un terminal et tapez&nbsp;:</p>

	<p><pre>sudo apt install silkaj</pre></p>
</section>

<section>
	<img class="card-img-top" src="/img/download/debian-logo.png" alt="Logo Debian" />

	<h3>
		Debian
	</h3>

	<p>Lancez un terminal et tapez&nbsp;:</p>

	<p><pre>sudo apt install silkaj</pre></p>
</section>

<section>
	<img class="card-img-top" src="/img/download/PyPI-logo.svg" alt="Logo PyPI" />

	<h3>
		PyPI
	</h3>

	<p>Pour installer Silkaj, lancez un terminal et tapez&nbsp;:</p>

	<p><pre>pip3 install silkaj --user</pre></p>

	<p>Pour mettre à jour Silkaj, lancez un terminal et tapez&nbsp;:</p>

	<p><pre>pip3 install silkaj --user --upgrade</pre></p>
</section>

<section>
	<a href="https://git.duniter.org/clients/python/silkaj">
		<img class="card-img-top" src="/img/download/gitlab-logo.svg" alt="Logo Source code" />
	</a>

	<h3>
		Code source
	</h3>

	<p>
		Voir le code source de Silkaj sur le GitLab
	</p>
		<p class="CTA-button">
			<a href="https://git.duniter.org/clients/python/silkaj">
				Voir le code source
			</a>
		</p>
</section>
</section>
