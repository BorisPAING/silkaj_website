Title: Silkaj - Client en ligne de commande pour Duniter
BodyId: home

<section id="showcase">
	<div>
		<figure id="home-app-screenshot">
			<!--
			<img src="/www/silkaj-website-php/i18n/fr_FR/contents/accueil/capture-d-ecran-Silkaj.png" 
				 alt="Capture d'écran de Silkaj" />
			-->
<pre><span></span><div>moul@duniter:~$ silkaj

	             @@@@@@@@@@@@@
         @@@     @         @@@
      @@@   @@       @@@@@@   @@.
     @@  @@@       @@@@@@@@@@@  @@
   @@  @@@       &amp;@@@@@@@@@@@@@  @@@
  @@  @@@       @@@@@@@@@#   @@@@ @@(
  @@ @@@@      @@@@@@@@@      @@@  @@
 @@  @@@      @@@@@@@@ @       @@@  @@
 @@  @@@      @@@@@@ @@@@       @@  @@ 
 @@  @@@@      @@@ @@@@@@@      @@  @@
  @@ @@@@*       @@@@@@@@@      @# @@
  @@  @@@@@    @@@@@@@@@@       @ ,@@
   @@  @@@@@ @@@@@@@@@@        @ ,@@
    @@@  @@@@@@@@@@@@        @  @@*
      @@@  @@@@@@@@        @  @@@
        @@@@   @@          @@@,
            @@@@@@@@@@@@@@@
</div></pre>
		</figure>

		<h2 id="USP">
			Client en ligne de commande performant et léger pour Duniter		</h2>

		<p class="CTA-button">
			<a href="telechargement">
				Installer Silkaj 0.2.0			</a>
		</p>


		<p id="licence">
			Silkaj est un logiciel libre<br />sous licence GNU AGPL-3.0		</p>
	</div>
</section>


<section class="features-list" id="features-list-1">
	<div>
		<h2>
			Silkaj : le client léger		</h2>
		
		<dl>
			<dt>
				Écrit en Python			</dt>

			<dd>
				Silkaj est écrit en Python3.			</dd>

			<dt>
				Offert			</dt>

			<dd>
				 Parce qu'il est codé avec amour par une communauté de gens qui croient aux monnaies libres en général (et à la Ğ1 en particulier), Silkaj vous est offert sans exiger de contre-partie. 			</dd>

			<dt>
				Libre			</dt>

			<dd>
				Silkaj est un logiciel libre.Vous êtes donc libre d'en consulter le code source (<a href="https://git.duniter.org/clients/python/silkaj">sur GitLab</a>), et de l'adapter à vos besoins pour, par exemple, lancer votre propre monnaie libre.			</dd>
		</dl>
	</div>
</section>

