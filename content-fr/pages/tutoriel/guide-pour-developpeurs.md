Title: Guide pour développeurs
Template: doc

# Installer Silkaj dans un environement de développement avec Poetry

## Installer libsodium

```bash
sudo apt install libsodium23 # Debian Buster
sudo apt install libsodium18 # Debian Stretch
sudo dnf install libsodium # Fedora
```

## Installer Poetry

Consultez les instructions sur la [documentation d'installation de Poetry](https://poetry.eustace.io/docs/#installation).

## Sur Debian Buster

```bash
sudo apt install python3-pip python3-venv
pip3 install poetry --user --pre
```

## Installer les dépendances et l'environnement Python

```bash
git clone https://git.duniter.org/clients/python/silkaj
cd silkaj
poetry install
```

## Lancer Silkaj

Depuis le dépôt Silkaj, entrez dans l'environnement de développement et lancez Silkaj :

```bash
poetry shell
./bin/silkaj
```

Il se peut que vous deviez entrez dans l'invite de commande de Poetry pour accéder aux outils de développement tels que pytest ou black.

## Rendre Silkaj accessible depuis n'importe où

Ajoutez l'alias suivant à votre configuration shell :

```bash
alias silkaj="cd /path/to/silkaj/silkaj && poetry run silkaj"
```
