Title: Installer Silkaj
Template: doc

# Installer Silkaj

## Sur Ubuntu 19.04

Ouvez un terminal et tapez :

```bash
sudo apt install silkaj
```

## Sur Debian Buster

Ouvez un terminal et tapez :

```bash
sudo apt install silkaj
```

## Avec PyPI

Pour l'installation, ouvez un terminal et tapez :

```bash
pip3 install silkaj --user
```

Pour faire les mises à jour, ouvez un terminal et tapez :

```bash
pip3 install silkaj --user --upgrade 
```
