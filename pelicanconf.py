#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'moul'
SITENAME = u'Silkaj'
SITEURL = ''

THEMES_PATH = "themes/"
THEME = THEMES_PATH + 'silkaj'
THEME_STATIC_DIRECTORY = 'theme'



DELETE_OUTPUT_DIRECTORY = True

PLUGIN_PATHS = ['plugins/']
PLUGINS = ['i18n_subsites', 'pelican-page-hierarchy', 'assets']


# BEGINS <i18n and paths>

JINJA_ENVIRONMENT = {
  'extensions': ['jinja2.ext.i18n']
}

DEFAULT_LANG = u'fr'
I18N_TEMPLATES_LANG = u'en'
I18N_GETTEXT_NEWSTYLE = True
I18N_GETTEXT_LOCALEDIR = THEME + '/translations'
I18N_GETTEXT_DOMAIN = 'silkaj'

PATH = 'shared-content'
STATIC_PATHS = ['img', '.']
READERS = {'html': None}
		
I18N_SUBSITES = {
    'fr': {
		'THEME_STATIC_DIR': '../' + THEME_STATIC_DIRECTORY, 
		'PAGE_URL': '{slug}/',
		'PAGE_SAVE_AS': '{slug}.html', 
		'PATH': 'content-fr', 
		'PAGE_PATHS': ['pages'], 
		'STATIC_PATHS': ['.'], 
        'STATIC_SAVE_AS': '{path}', 
        'STATIC_URL': '{path}', 
		'ARTICLE_PATHS': ['articles'],
		'PAGE_EXCLUDES': ['articles'], 
		'ARTICLE_EXCLUDES': ['.'], 
		'READERS': {},
        'ARTICLE_URL': 'blog/{slug}', 
        'ARTICLE_SAVE_AS': 'blog/{slug}.html', 
        'ARTICLE_LANG_URL': 'blog/{slug}', 
        'ARTICLE_LANG_SAVE_AS': 'blog/{slug}.html', 
        'DIRECT_TEMPLATES': ['blog'], 
        'INDEX_SAVE_AS': 'blog.html', 
        'PAGINATED_DIRECT_TEMPLATES': {'blog': 3}, 
    }, 
    'en': {
		'THEME_STATIC_DIR': '../' + THEME_STATIC_DIRECTORY, 
		'PAGE_URL': '{slug}/',
		'PAGE_SAVE_AS': '{slug}.html', 
		'PATH': 'content-en', 
		'PAGE_PATHS': ['pages'], 
		'STATIC_PATHS': ['.'], 
        'STATIC_SAVE_AS': '{path}', 
        'STATIC_URL': '{path}', 
		'ARTICLE_PATHS': ['posts'],
		'PAGE_EXCLUDES': ['posts'], 
		'ARTICLE_EXCLUDES': ['.'], 
		'READERS': {}, 
        'ARTICLE_URL': 'blog/{slug}', 
        'ARTICLE_SAVE_AS': 'blog/{slug}.html', 
        'ARTICLE_LANG_URL': 'blog/{slug}', 
        'ARTICLE_LANG_SAVE_AS': 'blog/{slug}.html', 
        'DIRECT_TEMPLATES': ['blog'], 
        'INDEX_SAVE_AS': 'blog.html', 
        'PAGINATED_DIRECT_TEMPLATES': {'blog': 3}, 
    }
}


PAGE_URL = ''
PAGE_SAVE_AS = ''
PAGE_LANG_URL = ''
PAGE_LANG_SAVE_AS = ''
ARTICLE_URL = ''
ARTICLE_SAVE_AS = ''
ARTICLE_LANG_URL = ''
ARTICLE_LANG_SAVE_AS = ''
DIRECT_TEMPLATES = []
INDEX_SAVE_AS = ''


SLUGIFY_SOURCE = 'basename' # 'title'

# ENDS </i18n and paths>




DISPLAY_PAGES_ON_MENU = True

MENUITEMS = [
             ['Home', ('/')], 
             ['Features', ('/features/') ], 
             ['Download', ('/download/')], 
             ['Tutorial', ('/tutorial/')], 
             ['News', ('/blog/')], 
             ['Support us!', ('/support-us/')], 
            ]

TIMEZONE = 'Europe/Paris'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True


