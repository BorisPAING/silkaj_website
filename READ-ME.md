# Silkaj's website

## Technologies used

- [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)
- [Pelican](https://docs.getpelican.com/en/) SSG
	- Pelican uses [Jinja2](https://jinja.palletsprojects.com/en/2.10.x/) template engine
	- [i18n subsite](https://github.com/getpelican/pelican-plugins/blob/master/i18n_subsites) plugin
	- [page hierarchy](https://github.com/akhayyat/pelican-page-hierarchy/) plugin
	- [assets](https://github.com/getpelican/pelican-plugins/tree/master/assets) plugin
	
## Useful pages

- Discover SSG (Static Site Generators) :
	- [part 1](https://about.gitlab.com/blog/2016/06/03/ssg-overview-gitlab-pages-part-1-dynamic-x-static/)
	- [part 2](https://about.gitlab.com/blog/2016/06/10/ssg-overview-gitlab-pages-part-2/)
	- [part 3](https://about.gitlab.com/blog/2016/06/17/ssg-overview-gitlab-pages-part-3-examples-ci/)
- [Localizing using Jinja2](https://github.com/getpelican/pelican-plugins/blob/master/i18n_subsites/localizing_using_jinja2.rst)