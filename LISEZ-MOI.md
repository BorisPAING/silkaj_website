# Site web de Silkaj

## Technologies utilisées

- [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)
- Le générateur de site statique (SSG) [Pelican](https://docs.getpelican.com/en/)
	- Pelican utilise le moteur de template [Jinja2](https://jinja.palletsprojects.com/en/2.10.x/)
	- L'extension [i18n subsite](https://github.com/getpelican/pelican-plugins/blob/master/i18n_subsites)
	- L'extension [page hierarchy](https://github.com/akhayyat/pelican-page-hierarchy/)
	- L'extension [assets](https://github.com/getpelican/pelican-plugins/tree/master/assets)
	
## Pages utiles

- Découvrir les générateurs de site statiques :
	- [partie 1](https://about.gitlab.com/blog/2016/06/03/ssg-overview-gitlab-pages-part-1-dynamic-x-static/)
	- [partie 2](https://about.gitlab.com/blog/2016/06/10/ssg-overview-gitlab-pages-part-2/)
	- [partie 3](https://about.gitlab.com/blog/2016/06/17/ssg-overview-gitlab-pages-part-3-examples-ci/)
- [Localizing using Jinja2](https://github.com/getpelican/pelican-plugins/blob/master/i18n_subsites/localizing_using_jinja2.rst)
- [Apprendre à gérer le fichier .gitlab-ci.yml](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_four.html)

## Ce qu'on pourrait faire

- Utiliser Webassets pour compiler et minifier les javascripts
- Utiliser Webassets pour compiler et minifier les CSS
- Essayer d'utiliser FILENAME_METADATA pour pouvoir avoir les ressources statiques (.png) dans le dossier pages : http://docs.getpelican.com/en/latest/settings.html#metadata
- Chercher un moyen d'afficher la bordure sous lien Blog du menu
- Ajouter un fil d'Ariane à chaque page de tutoriel
- Ajouter les pages sœurs en bas de chaque page de tutoriel
- Chercher comment gérer les sessions en Javascript puis réfléchir à un script pour détecter la langue du visiteur lors de sa première visite et le rediriger vers le site idoine.
- Regarder s'il n'y a pas moyen de lier les traductions entre elles (peut-être une boucle for qui parcourt toutes les pages et affiche celles qui ont le même attribut name ?)
- Ajouter les meta dans le <head /> :
	- données og-graph
	- lang alternatives
	- meta description
	